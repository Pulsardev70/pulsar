Pulsar (PULSE)
===================
Distributed under the MIT/X11 software license.

Copyright 2009-2015 The Bitcoin developers

Copyright 2013 PPCoin developers
Copyright 2013 NovaCoin developers
Copyright 2014 BitcoinDark developers
Copyright 2015 Pulsar developers

Intro
-----
Pulsar is a free open source project derived from Bitcoin. Pulsar has a dynamic Proof of Work and Proof of Stake system, it's goals are to promote a fair community distribution, market relatively scarce goods exclusively through the PULSE cryptocurrency, and implement innovative features in order to facilitate an increased user base. Please check https://bitcointalk.org/index.php?topic=1021029.0 for the latest news.

Launch: Open Competition Distribution July 5, 2015

Ticker: PULSE

Algorithm: SHA256

DefaultPort = 43207

RPCPort = 43208

60 Seconds Per Block

160 Blocks to Confirm

20MB Blocksize

Block Reward Schedule - Proof of Work Phase
-------------------------------------------
1-100			750 PULSE

101-500			500 PULSE

501-1,000		350 PULSE

1,001-1,500		250 PULSE

1,501-1,550		350 PULSE

1,551-2,000		250 PULSE

2,001-4,500		150 PULSE

*POS begins at block 2,501	88 PULSE

Max Total POW Coins                			1,080,000 PULSE

Block Reward Schedule - Dynamic Proof of Stake Phase
-------------------------------------------
4,501-7,380                                                     	10 PULSE

7,381                                                                 	808 PULSE

7,382-10,260                                                   	10 PULSE

10,261-11,700                                			30 PULSE

11,701                                                               	808 PULSE

11,702-13,140                                               	 	30 PULSE

13,141-13,200                                               	 	88 PULSE

13,201-14,640                                                		30 PULSE

14,641                                                               	808 PULSE

14,642-16,080                                                		30 PULSE

16,081-16,140                                                		88 PULSE

16,141-19,020                                                		10 PULSE

19,021                                                               	8,080 PULSE

19,022-21,900                                               	 	10 PULSE

21,901-23,340                                                		50 PULSE

23,341                                                               	808 PULSE

23,342-24,780                                                		50 PULSE

24,781-550,380                                              		2.6 PULSE

550,381-1,075,980                                        		2.7 PULSE

1,075,981-1,601,580                                    		2.9 PULSE

Max Total POW / POS Coins                		1,533,742 PULSE

1,601,581+                                                      	9% ANNUAL

Dynamic POS lasts approximately two weeks. End Block 27,780.

Total Approx Coins from month of Dynamic POS: 450,000.

Total Approx Total Coins 1.3m.

After block 24,780 POS goes to approx 9% yearly.

See bitcointalk.org Pulsar thread for more info.
